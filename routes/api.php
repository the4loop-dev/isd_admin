<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => 'v1'], function () {

    Route::get('login', function(){
        return response()->json([
            'data'=>'',
            'state'=>false,
            'msg'=>'You Must login First'
        ]);
    })->name('login');

    Route::post('/users/register', 'Api\AuthController@register');
    Route::post('/users/register/confirm', 'Api\AuthController@RegisterConfirmationCode');
    Route::post('/users/login', 'Api\AuthController@login');
    Route::post('/users/forgot/password', 'Api\AuthController@forgotpassword');
    Route::post('/users/forgot/confirmation', 'Api\AuthController@ConfirmationCode');
    Route::post('/users/forgot/change', 'Api\AuthController@ChangePassword');
    Route::get('/users/{id}', 'Api\UsersController@Show');
    Route::get('/courts/{id}', 'Api\CourtsController@Show');
    Route::get('/sports', 'Api\CourtsController@Sport');
    Route::get('/sports/{id}', 'Api\CourtsController@SportDetails');
    Route::post('/check/courts', 'Api\CourtsController@CheckTime');
    Route::get('/products/{id}', 'Api\CourtsController@Products');
    Route::get('/sport/{id}', 'Api\CourtsController@SportName');
    Route::post('/error', 'Api\AuthController@Error');
    Route::post('/user/update/token', 'Api\UsersController@UpdateToken');
    Route::post('/user/sendNotifcation', 'Api\UsersController@SendNotifcation');
    Route::post('/user/all/sendNotifcation', 'Api\UsersController@SendNotifcationAll');
    Route::post('/checkAvailability', 'Api\CourtsController@CheckAvailability');
    Route::get('/pay', 'Api\PaymentController@pay');
    Route::get('/pay/agian', 'Api\PaymentController@PayAgain');
    Route::get('/pay/agian/addCvv', 'Api\PaymentController@PayAgainCvv');
    Route::get('/mobilePaymentSuccess', 'Api\PaymentController@mobilePaymentSuccess');
    Route::get('/mobilePaymentFail', 'Api\PaymentController@pay');
    Route::get('/mobilePaymentDeclined', 'Api\PaymentController@mobilePaymentDeclined');
    Route::post('/check/favourit', 'Api\CourtsController@CheckFavourit');
    Route::get('/user/book/file/{id}', 'Api\BookingController@BookFile');
    Route::get('/product/{id}', 'Api\BookingController@ProductDetails');

    Route::post('/academy/check/', 'Api\AcademyController@Check');
    Route::get('/academy/classes/{id}', 'Api\AcademyController@Classes');
    Route::post('/academy/age/check', 'Api\AcademyController@CheckAgeGroup');
    Route::post('/academy/packages', 'Api\AcademyController@Packages');
    Route::post('/academy/package', 'Api\AcademyController@PackagesDetails');
    Route::post('/academy/calculate/amount', 'Api\AcademyController@CalculateAmount');
    Route::get('/academy/vacations/', 'Api\AcademyController@Vacations');
    Route::get('/academy/terms/', 'Api\AcademyController@Terms');
    Route::post('/academy/packages/check', 'Api\AcademyController@MyPackages');
    Route::get('/academy/comments/subscriber', 'Api\AcademyController@SubscriberComments');
    Route::post('/academy/subscriptions/add', 'Api\AcademyController@StoreSubscriptions');
    Route::post('/academy/coach/email', 'Api\AcademyController@SendEmailcoach');
    Route::get('/academy/invoice/download/{id}', 'Api\AcademyController@DownloadInvoice');
    Route::post('/academy/invoice/email', 'Api\AcademyController@SendEmailInvoice');
    Route::get('/academy/{id}', 'Api\AcademyController@AcademySingle');
        Route::post('/user/show', 'Api\UsersController@UserDetails');


    Route::group(['middleware' => ['auth:api']], function(){
        Route::get('/academy/subscriptions/all', 'Api\AcademyController@Subscriptions');
        Route::get('/user', 'Api\UsersController@User');
        Route::get('/users', 'Api\UsersController@Users');
        Route::post('/users/edite', 'Api\UsersController@EditeUser');
        Route::get('/user/like', 'Api\UsersController@UsersLiked');
        Route::post('/user/add/like', 'Api\UsersController@UsersAddFavourite');
        Route::post('/user/add/review', 'Api\UsersController@UsersAddReview');
        Route::get('/user/review', 'Api\UsersController@UsersReview');
        Route::post('/book/add', 'Api\BookingController@AdBook');
        Route::get('/user/book/up', 'Api\BookingController@UsersBookUp');
        Route::get('/user/book/past', 'Api\BookingController@UsersBookPast');
        Route::get('/user/book/{id}', 'Api\BookingController@Show');
        Route::post('/user/book/email', 'Api\BookingController@BookEmail');
        Route::post('/book/invoices/email', 'Api\BookingController@SendInvoicesToMail');
        Route::get('/user/cards', 'Api\UsersController@UsersCards');
        Route::post('/user/card/delete', 'Api\UsersController@UsersCardDelete');

        
    });



});