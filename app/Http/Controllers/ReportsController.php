<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Court_times;
use App\Booking;
use Carbon;
use DB;
use App\Exports\BookingExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Subscription;
use App\Academy;
use App\Payment;

class ReportsController  extends Controller
{

    public function Booking()
    {
      $data = Booking::whereDate('created_at', Carbon\Carbon::today())->with('user','products')->get();
      return view('reports.booking')->with('data',$data);
    }

    public function Schedule()
    {
      $q =  Booking::with('user','products'); 
      if(isset($_GET['court']) && $_GET['court'] > 0){
        $q->where('pitch', $_GET['court']);
      }
      if(isset($_GET['users']) && $_GET['users'] > 0){
          $q->where('client_id', $_GET['users']);
      }
      if(isset($_GET['from']) && $_GET['from'] > 0 && isset($_GET['to']) && $_GET['to'] > 0){
          $q->whereBetween('created_at', [date($_GET['from']), date($_GET['to'])]);
      }        
      $data = $q->get();
      $sum = $q->sum('total');
      $sum2 = 0;
      foreach ($data as $key) {
        $v =  $key->vat * $key->total / 100;
        $sum2 += $key->total + $v;
      }
      return view('reports.schedule')->with('data',$data)->with('sum',$sum)->with('sum2',$sum2);
    }

    public function Finance()
    {
      $q =  Booking::with('user','products'); 
      if(isset($_GET['court']) && $_GET['court'] > 0){
        $q->where('pitch', $_GET['court']);
      }
      if(isset($_GET['users']) && $_GET['users'] > 0){
          $q->where('client_id', $_GET['users']);
      }
      if(isset($_GET['from']) && $_GET['from'] > 0 && isset($_GET['to']) && $_GET['to'] > 0){
          $q->whereBetween('created_at', [date($_GET['from']), date($_GET['to'])]);
      }        
      $data = $q->get();
      $sum = $q->sum('total');
      $sum2 = 0;
      foreach ($data as $key) {
        $v =  $key->vat * $key->total / 100;
        $sum2 += $key->total + $v;
      }
      return view('reports.finance')->with('data',$data)->with('sum',$sum)->with('sum2',$sum2);
    }    


    public function Academy()
    {
      $q =  Payment::where('rent',2)->with('Subscription'); 
      if(isset($_GET['sport']) && $_GET['sport'] > 0){
        $sport =  $_GET['sport'];
        $q->whereHas('academies', function ($q) use ($sport) {
            $q->where('sport', $sport);
        });        
      }       

      if(isset($_GET['age']) && $_GET['age'] > 0){
        $age =  $_GET['age'];
        $q->whereHas('academies', function ($q) use ($age) {
            $q->where('age', $age);
        });        
      } 

      if(isset($_GET['academies']) && $_GET['academies'] > 0){
        $academies =  $_GET['academies'];
        $q->whereHas('academies', function ($q) use ($academies) {
            $q->where('academies', $academies);
        });        
      } 

      if(isset($_GET['from']) && $_GET['from'] > 0 && isset($_GET['to']) && $_GET['to'] > 0){
          $q->whereBetween('created_at', [date($_GET['from']), date($_GET['to'])]);
      } 
      
      $data = $q->get();
      $sum = 0;
      $sum2 = 0;
      return view('reports.academy')->with('data',$data)->with('sum',$sum)->with('sum2',$sum2);
    }
}