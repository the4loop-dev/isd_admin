<?php



namespace App\Http\Controllers\Auth;



use App\Http\Controllers\Controller;

use App\Providers\RouteServiceProvider;

use App\User;

use App\Player;

use App\SignUp;

use Illuminate\Foundation\Auth\RegistersUsers;

use Illuminate\Support\Facades\Hash;

use Illuminate\Support\Facades\Validator;



class RegisterController extends Controller

{

    /*

    |--------------------------------------------------------------------------

    | Register Controller

    |--------------------------------------------------------------------------

    |

    | This controller handles the registration of new users as well as their

    | validation and creation. By default this controller uses a trait to

    | provide this functionality without requiring any additional code.

    |

    */



    use RegistersUsers;



    /**

     * Where to redirect users after registration.

     *

     * @var string

     */

    protected $redirectTo = RouteServiceProvider::HOME;



    /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function __construct()

    {

        $this->middleware('guest');

    }



    /**

     * Get a validator for an incoming registration request.

     *

     * @param  array  $data

     * @return \Illuminate\Contracts\Validation\Validator

     */

    public function showRegistrationForm()
    {
        return view('auth.register');
    }

    protected function validator(array $data)

    {

        return Validator::make($data, [

            'name' => ['required', 'string', 'max:255'],

            'email' => ['string', 'email', 'max:255', 'unique:users'],

        ]);

    }



    /**

     * Create a new user instance after a valid registration.

     *

     * @param  array  $data

     * @return \App\User

     */

    protected function create(array $data)

    {

     

      if( $data['form_type_id']==TYPE_VENU){

         $date=date('Y-m-d',strtotime($data['date_of_requirement']));

         $time=date('H:i:s',strtotime($data['time_of_requirement']));

        

         $user = User::create([

            'name' => $data['name'],

            'email' => $data['email'],

            'mobile_no' => $data['mobile'],

            'is_active' => STATUS_DISABLED,



        ]);

        $user->save();

        

        $signUps=SignUp::create([

            'sport_id' => $data['sport_id'],

            'user_id' => $user->id,

            'form_type_id' => $data['form_type_id'],

            'date_of_requirment' => $date,

            'time_of_requirment' => $time,

            'is_active' => STATUS_DISABLED,



        ]);

         $signUps->save();

         

      }



      elseif( $data['form_type_id']==TYPE_ACADEMY){

         $user = User::create([

            'name' => $data['name'],

            'email' => $data['email'],

            'mobile_no' => $data['mobile'],

            'is_active' => STATUS_DISABLED,



        ]);

        $user->save();

          $player = Player::create([

            'name' => $data['player_name'],

            'dob' => $data['dob'],

            'is_active' => STATUS_DISABLED,



        ]);

        $player->save();

        $signUps=SignUp::create([

            'sport_id' => $data['sport_id'],

            'user_id' => $user->id,

            'player_id' => $player->id,

            'form_type_id' => $data['form_type_id'],

            'is_active' => STATUS_ENABLED,



        ]);

        $signUps->save();

      }

    }

}

