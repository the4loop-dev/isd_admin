<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CourtTime;
use App\Court;
use DateTime;
use App\Court_books;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Events\BreadDataAdded;
use App\Court_times;
use Carbon;
use App\OrderProduct;
use App\Product;

class OrderProductsController  extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{

    public function store(Request $request)
    {
        $rand = rand();
        for($i= 0; $i < count($request->title_product); $i++){
            $OrderProduct = new OrderProduct();
            $product = Product::where('id',$request->title_product[$i])->first();
            $OrderProduct->title = $product->title;
            $OrderProduct->price = $request->price_product[$i];
            $OrderProduct->total = $request->count_product[$i] * $request->price_product[$i];
            $OrderProduct->count = $request->count_product[$i];
            $OrderProduct->user_id  = $request->user_id;
            $OrderProduct->uid  = $rand;
            $OrderProduct->save();
            $product->decrement('stock', $request->title_product[$i]);
        }      
        return redirect('admin/order-products');
    
    }

}