<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CourtTime;
use App\Court;
use DateTime;
use App\Court_books;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Events\BreadDataAdded;
use App\Court_times;
use Carbon;
use App\Booking;
use Auth;
use App\Subscription;
use App\Classes;
use App\User;
use Hash;
use Mail;
use DB;
use PDF;

class SubscriptionsController  extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{

    public function index(Request $request)
    {
        // GET THE SLUG, ex. 'posts', 'pages', etc.
        $slug = $this->getSlug($request);

        // GET THE DataType based on the slug
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('browse', app($dataType->model_name));

        $getter = $dataType->server_side ? 'paginate' : 'get';

        $search = (object) ['value' => $request->get('s'), 'key' => $request->get('key'), 'filter' => $request->get('filter')];

        $searchNames = [];
        if ($dataType->server_side) {
            $searchable = SchemaManager::describeTable(app($dataType->model_name)->getTable())->pluck('name')->toArray();
            $dataRow = Voyager::model('DataRow')->whereDataTypeId($dataType->id)->get();
            foreach ($searchable as $key => $value) {
                $field = $dataRow->where('field', $value)->first();
                $displayName = ucwords(str_replace('_', ' ', $value));
                if ($field !== null) {
                    $displayName = $field->getTranslatedAttribute('display_name');
                }
                $searchNames[$value] = $displayName;
            }
        }

        $orderBy = $request->get('order_by', $dataType->order_column);
        $sortOrder = $request->get('sort_order', $dataType->order_direction);
        $usesSoftDeletes = false;
        $showSoftDeleted = false;

        // Next Get or Paginate the actual content from the MODEL that corresponds to the slug DataType
        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
                $query = $model->{$dataType->scope}();
            } else {
                $query = $model::select('*');
            }
            if(isset($_GET['paid']) && $_GET['paid'] > 0 ){
                $query = $query->where('paid', 1);
            }               
            if(isset($_GET['not_paid']) && $_GET['not_paid'] > 0 ){
                $query = $query->where('paid', 0);
            }    
            if(isset($_GET['published']) && $_GET['published'] > 0 ){
                $query = $query->where('status', 1);
            }  
            if(isset($_GET['saved']) && $_GET['saved'] > 0 ){
                $query = $query->where('status', 0);
            }            
            if(isset($_GET['academy']) && $_GET['academy'] > 0 ){
                $query = $query->where('academy',$_GET['academy']);
            }
            if(isset($_GET['subscriber']) && $_GET['subscriber'] > 0 ){
                $query = $query->where('subscriber', $_GET['subscriber']);
            }
            if(isset($_GET['age']) && $_GET['age'] > 0 ){
                $query = $query->where('age', $_GET['age']);
            }       
            if(isset($_GET['team']) && $_GET['team'] > 0 ){
                $query = $query->where('team', $_GET['team']);
            }  
            if(isset($_GET['term']) && $_GET['term'] > 0 ){
                $term = json_encode($_GET['term']);
                $query = $query->where('terms','LIKE', $term);
                
            }                               
            if(isset($_GET['package']) && $_GET['package'] > 0 ){
                $query = $query->where('package', $_GET['package']);
            }  
            if(isset($_GET['class']) && $_GET['class'] > 0 ){
                $query = $query->where('class', $_GET['class']);
            }  
            if(isset($_GET['level']) && $_GET['level'] > 0 ){
                $query = $query->where('level', $_GET['level']);
            }  
            if(isset($_GET['num_days']) && $_GET['num_days'] > 0 ){
                $query = $query->where('num_days', $_GET['num_days']);
            }                                                  

            if ($model && in_array(SoftDeletes::class, class_uses_recursive($model)) && Auth::user()->can('delete', app($dataType->model_name))) {
                $usesSoftDeletes = true;

                if ($request->get('showSoftDeleted')) {
                    $showSoftDeleted = true;
                    $query = $query->withTrashed();
                }
            }

            // If a column has a relationship associated with it, we do not want to show that field
            $this->removeRelationshipField($dataType, 'browse');

            if ($search->value != '' && $search->key && $search->filter) {
                $search_filter = ($search->filter == 'equals') ? '=' : 'LIKE';
                $search_value = ($search->filter == 'equals') ? $search->value : '%'.$search->value.'%';
                $query->where($search->key, $search_filter, $search_value);
            }

            if ($orderBy && in_array($orderBy, $dataType->fields())) {
                $querySortOrder = (!empty($sortOrder)) ? $sortOrder : 'desc';
                $dataTypeContent = call_user_func([
                    $query->orderBy($orderBy, $querySortOrder),
                    $getter,
                ]);
            } elseif ($model->timestamps) {
                $dataTypeContent = call_user_func([$query->latest($model::CREATED_AT), $getter]);
            } else {
                $dataTypeContent = call_user_func([$query->orderBy($model->getKeyName(), 'DESC'), $getter]);
            }

            // Replace relationships' keys for labels and create READ links if a slug is provided.
            $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType);
        } else {
            // If Model doesn't exist, get data from table name
            $dataTypeContent = call_user_func([DB::table($dataType->name), $getter]);
            $model = false;
        }

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($model);

        // Eagerload Relations
        $this->eagerLoadRelations($dataTypeContent, $dataType, 'browse', $isModelTranslatable);

        // Check if server side pagination is enabled
        $isServerSide = isset($dataType->server_side) && $dataType->server_side;

        // Check if a default search key is set
        $defaultSearchKey = $dataType->default_search_key ?? null;

        // Actions
        $actions = [];
        if (!empty($dataTypeContent->first())) {
            foreach (Voyager::actions() as $action) {
                $action = new $action($dataType, $dataTypeContent->first());

                if ($action->shouldActionDisplayOnDataType()) {
                    $actions[] = $action;
                }
            }
        }

        // Define showCheckboxColumn
        $showCheckboxColumn = false;
        if (Auth::user()->can('delete', app($dataType->model_name))) {
            $showCheckboxColumn = true;
        } else {
            foreach ($actions as $action) {
                if (method_exists($action, 'massAction')) {
                    $showCheckboxColumn = true;
                }
            }
        }

        // Define orderColumn
        $orderColumn = [];
        if ($orderBy) {
            $index = $dataType->browseRows->where('field', $orderBy)->keys()->first() + ($showCheckboxColumn ? 1 : 0);
            $orderColumn = [[$index, $sortOrder ?? 'desc']];
        }

        $view = 'voyager::bread.browse';

        if (view()->exists("voyager::$slug.browse")) {
            $view = "voyager::$slug.browse";
        }


        return Voyager::view($view, compact(
            'actions',
            'dataType',
            'dataTypeContent',
            'isModelTranslatable',
            'search',
            'orderBy',
            'orderColumn',
            'sortOrder',
            'searchNames',
            'isServerSide',
            'defaultSearchKey',
            'usesSoftDeletes',
            'showSoftDeleted',
            'showCheckboxColumn'
        ));
    }

    public function store(Request $request){
        //dd($request->all());
        foreach ($request->subscription_belongstomany_classe_relationship as $key => $value) {
            $count = Subscription::where('class',$value)->count();
            $capacity = Classes::find($value)->first();
            $i = $capacity['capacity'] - $count; 
        }


        $age = date("Y") - date("Y", strtotime($request->age));
        $Subscription = new Subscription();
        $Subscription->academy = $request->academy;
        $Subscription->user_id = $request->user_id;
        $Subscription->start_date = $request->start_date;
        $Subscription->enf_date = $request->enf_date;
        $Subscription->subscription_fees  = $request->subscription_fees_total;
        $Subscription->age = $age;
        $Subscription->date_birth = $request->age;
        $Subscription->class = $request->class;
        $Subscription->num_days = json_encode($request->num_days);
        $Subscription->terms = json_encode($request->subscription_belongstomany_term_relationship);
        $Subscription->package  = $request->package;
        $Subscription->type_num_days  = $request->type_num_days;
        $Subscription->city  = $request->city;
        $Subscription->discount  = $request->subscription_fees_discount;
        $Subscription->save();

        foreach ($request->subscription_belongstomany_term_relationship as $key => $value) {
            DB::table('povite_subscriptions_terms')->insert(
                ['subscription_id' => $Subscription->id, 
                 'term_id' => $value]
            );
        }

      foreach ($request->subscription_belongstomany_classe_relationship as $key => $value) {
            DB::table('povite_subscriptions_classes')->insert(
                ['subscription_id' => $Subscription->id, 
                 'classes_id' => $value]
            );
        }
        
        $SubscriberCheck      = User::where('id',$request['subscriber'])->count();
        if($SubscriberCheck == 0){
            $phone = User::where('id',$request->user_id)->value('contacts');
            $user = new User();
            $user->relative = $request->user_id;
            $user->name = $request->subscriber;
            $user->type = 2;
            $user->subscription = $Subscription->id;
            $user->academy = $request->academy;
            $user->email = $request->name.''.$request->user."2021@gmail.com";
            $user->password = Hash::make("123456");
            $user->contacts = $phone;
            $user->age_group = $age;
            $user->date = $request->age;        
            $user->save();
            $Subscription = Subscription::find($Subscription->id);
            $Subscription->subscriber = $user->id;
            $Subscription->save();
        }else{
            $Subscription = Subscription::find($Subscription->id);
            $Subscription->subscriber = $request['subscriber'];
            $Subscription->save();
        }

        if($i <= 2){
            $this->SendEmailToAdmin($request->class,$age,$request->academy);       
        } 
        $this->SendEmailToAdminAttention($Subscription->id,$request->academy);   
        //$this->SendEmailClient($request->user_id,$Subscription->id); 
        return redirect('admin/subscriptions/');      
    }

    public function SendEmailClient($id,$Subscription){
        $email =  DB::table('users')->where('id',$id)->value('email');
        Mail::to($email)->send(new \App\Mail\NewSubscription($Subscription));  
    }

    public function SendEmailToAdmin($class,$age,$academy){
        $to_email = "myisdalerts@isddubai.com";
        Mail::to($to_email)->send(new \App\Mail\AlertClass($class,$academy));  
        return response()->json([
            'status'=>200,
        ]); 
    }

    public function SendEmailToAdminAttention($subscription,$academy){
        $to_email = "myisdalerts@isddubai.com";
        Mail::to($to_email)->send(new \App\Mail\AlertClassAttention($subscription,$academy));  
        return response()->json([
            'status'=>200,
        ]);
    }

    public function update(Request $request,$id){
        $Subscription = Subscription::find($id);
        $Subscription->num_days = json_encode($request->num_days);
        $Subscription->level    = $request->level;
        $Subscription->team     = $request->team;
        $Subscription->save();
        return redirect('admin/subscriptions');
    }

    public function PublishCustomer($user_id,$id){
        Subscription::find($id)->update(['status', '1']);
        $this->SendEmailClient($user_id,$id); 
         return redirect()->back();
    }
    
    
   public function ExportReceiptSubscription ($id){
        $subscription = Subscription::find($id);
        $details = ['id' => $id];
        //return view('emails.invoice_academy',compact('details'));
        $pdf = PDF::loadView('emails.invoice_academy',compact('details'));
        return $pdf->download('invoice'.$id.'.pdf');
    }
        
    
}

?>