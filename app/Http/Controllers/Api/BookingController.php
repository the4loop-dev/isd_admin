<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Hash;
use Validator;
use Auth;
use DB;
use Mail;
use App\Booking;
use App\Court;
use App\OrderProduct;
use App\Product;
use Carbon;
use PDF;

class BookingController extends Controller
{

    public function list(Request $request)
    {
        $data = Booking::where('active',1)->orderBy('id', 'desc')->get();
        return response()->json([
            'status'=>200,
            'data'=>$data
        ]);
    }

    public function Show(Request $request,$id)
    {
        $data = Booking::where('active',1)->where('id',$id)->first();
        return response()->json([
            'status'=>200,
            'data'=>$data
        ]);
    }

    public function AdBook(Request $request)
    {
        $size =  Court::where('id',$request->pitch)->value('size');
        $namesport = DB::table('sports')->where('id',$request->sport)->value('name');
        $booking = new Booking();
        $booking->date = $request->date;
        $booking->from = $request->from;
        $booking->to = $request->to;
        $booking->pitch = $request->pitch;
        $booking->size = $size;
        $booking->price = $request->price;
        $booking->dicsount = 0;
        $booking->total = $request->price;
        $booking->client_id = $request->client_id;
        $booking->status = 0;
        $booking->penalty = 0;
        $booking->sports = $request->sport;
        $booking->code = rand ( 10000 , 99999 );
        $vat =  $request->price - round($request->price / 1.05);
        $booking->vat = $vat;
        $booking->method = 0;        
        $booking->save();
        if($request->products > 0){
            for($i= 0; $i < count($request->products); $i++){
                $OrderProduct = new OrderProduct();
                $OrderProduct->title = $request->products[$i]["title"];
                $OrderProduct->price = $request->products[$i]["price"];
                $OrderProduct->total = $request->products[$i]["price"] * $request->products[$i]["count"];
                $OrderProduct->count = $request->products[$i]["count"];
                $OrderProduct->book_id  = $booking->id;
                $OrderProduct->save();
                $product = Product::where('id',$request->products[$i]["id"])->first();
                $product->decrement('stock', $request->products[$i]["count"]);
            }  
        }              
        return response()->json([
            'status'=>200,
            'data' => $booking,
            'sport' => $namesport,
            'msg' => "Data Insert Success",
        ]);    
    }

    public function  SendInvoicesToMail(Request $request){
        $booking = Booking::find($request->id);
        $email = DB::table('users')->where('id',$booking->client_id)->value('email');
        $name = DB::table('users')->where('id',$booking->client_id)->value('name');
        $this->MakePdfInvoice($booking->id);
        $this->MakePdfReceipt($booking->id,$name,$email);
        $details = [
            'code' => $booking->code,
            'date' => $booking->date,
            'from' => $booking->from,
            'to' => $booking->to,
            'pitch' => $booking->pitch,
            'sports' => $booking->sports,
            'total' => $booking->total,
            'id' => $booking->id,
            'name' => $name,
            'email' => $email,
            'date' => $booking->created_at,
        ];
        Mail::to($email)->send(new \App\Mail\Booking($details));         
 
    }

    public function MakePdfInvoice($id){
        $booking = Booking::find($id);
        $details = [
            'created_at'  => $booking->created_at,
            'date'  => $booking->date,
            'from'  => $booking->from,
            'to'    => $booking->to,
            'pitch' => $booking->pitch,
            'sports'=> $booking->sports,
            'total' => $booking->total,
            'client_id' => $booking->client_id,
            'code'  => $booking->code,
            'id'  => $id,
        ];
        $pdf = PDF::loadView('emails.tax',compact('details'));
        return $pdf->save('attached/tax_'.$id.'.pdf');
    }

    public function MakePdfReceipt($id,$name,$email){
        $booking = Booking::find($id);
        $details = [
            'code'  => $booking->code,
            'date'  => $booking->date,
            'from'  => $booking->from,
            'to'    => $booking->to,
            'pitch' => $booking->pitch,
            'sports'=> $booking->sports,
            'total' => $booking->total,
            'name' => $name,
            'email' => $email,
            'date' => $booking->created_at,            
        ];
        $pdf = PDF::loadView('emails.invoice',compact('details'));
        return $pdf->save('attached/invoice_'.$id.'.pdf');
    }

    public function UsersBookUp(){
        $data = Booking::where('client_id',Auth::user()->id)->whereDate('created_at', '>=',Carbon\Carbon::today())->get();        
        $response['status'] = 200;
        $response['data'] = $data;
        return $response;
    }

    public function UsersBookPast(){
        $data = Booking::where('client_id',Auth::user()->id)->whereDate('created_at', '<=',Carbon\Carbon::today())->get();        
        $response['status'] = 200;
        $response['data'] = $data;
        return $response;
    }

    public function BookEmail(Request $request)
    {
        $booking = Booking::find($request->id);
        $name = DB::table('users')->where('id',$booking->client_id)->value('name');
        $email = DB::table('users')->where('id',$booking->client_id)->value('email');
        $details = [
            'code' => $booking->code,
            'date' => $booking->date,
            'from' => $booking->from,
            'to' => $booking->to,
            'pitch' => $booking->pitch,
            'sports' => $booking->sports,
            'total' => $booking->total,
            'name' => $name,
            'email' => $email,
            'date' => $booking->created_at,
            'user'  => $booking->client_id,
            'id'  => $request->id,
        ];
        Mail::to($email)->send(new \App\Mail\Invoice($details));        
        return response()->json([
            'status'=>200,
            'msg' => "Email Sent Successfully",
        ]);           
    }

    public function BookFile($id)
    {
        $booking = Booking::find($id);
        $details = [
            'code'  => $booking->code,
            'date'  => $booking->date,
            'from'  => $booking->from,
            'to'    => $booking->to,
            'pitch' => $booking->pitch,
            'sports'=> $booking->sports,
            'total' => $booking->total,
            'user'  => $booking->client_id,
            'id'  => $id,
        ];
        $pdf = PDF::loadView('emails.invoice',compact('details'));
        return $pdf->download('Book.pdf');
    }

    public function ProductDetails($id){
        $product = Product::find($id);
        return response()->json([
            'data'=>$product,
        ]);
    }
}