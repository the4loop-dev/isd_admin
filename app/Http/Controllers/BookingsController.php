<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CourtTime;
use App\Court;
use DateTime;
use App\Court_books;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Events\BreadDataAdded;
use App\Court_times;
use App\Booking;
use App\OrderProduct;
use App\Product;
use Carbon;
use Illuminate\Support\Facades\Auth;
use App;
use PDF;
use App\Payment;
use DB;
use Mail;
use App\User;
use App\Subscription;
class BookingsController  extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{

    public function index(Request $request)
    {
        // GET THE SLUG, ex. 'posts', 'pages', etc.
        $slug = $this->getSlug($request);
        // GET THE DataType based on the slug
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('browse', app($dataType->model_name));

        $getter = $dataType->server_side ? 'paginate' : 'get';

        $search = (object) ['value' => $request->get('s'), 'key' => $request->get('key'), 'filter' => $request->get('filter')];

        $searchNames = [];
        if ($dataType->server_side) {
            $searchable = SchemaManager::describeTable(app($dataType->model_name)->getTable())->pluck('name')->toArray();
            $dataRow = Voyager::model('DataRow')->whereDataTypeId($dataType->id)->get();
            foreach ($searchable as $key => $value) {
                $field = $dataRow->where('field', $value)->first();
                $displayName = ucwords(str_replace('_', ' ', $value));
                if ($field !== null) {
                    $displayName = $field->getTranslatedAttribute('display_name');
                }
                $searchNames[$value] = $displayName;
            }
        }

        $orderBy = $request->get('order_by', $dataType->order_column);
        $sortOrder = $request->get('sort_order', $dataType->order_direction);
        $usesSoftDeletes = false;
        $showSoftDeleted = false;

        // Next Get or Paginate the actual content from the MODEL that corresponds to the slug DataType
        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
                $query = $model->{$dataType->scope}();
            } else {
                $query = $model::select('*');
            }
            if(isset($_GET['users']) && $_GET['users'] > 0 ){
                $query = $query->where('client_id',$_GET['users']);
            }
            if(isset($_GET['court']) && $_GET['court'] > 0 ){
                $query = $query->where('pitch',$_GET['court']);
            }
            if(isset($_GET['sport']) && $_GET['sport'] > 0 ){
                $query = $query->where('sports',  'LIKE', "%{$_GET['sport']}%");
            }
            if(!isset($_GET['not_paid'])){
                $query = $query->where('status', 1);
            }            
            if(isset($_GET['not_paid']) && $_GET['not_paid'] > 0 ){
                $query = $query->where('status', 2)->orWhere('status', 0);
            }           
            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($model && in_array(SoftDeletes::class, class_uses_recursive($model)) && Auth::user()->can('delete', app($dataType->model_name))) {
                $usesSoftDeletes = true;

                if ($request->get('showSoftDeleted')) {
                    $showSoftDeleted = true;
                    $query = $query->withTrashed();
                }
            }

            // If a column has a relationship associated with it, we do not want to show that field
            $this->removeRelationshipField($dataType, 'browse');

            if ($search->value != '' && $search->key && $search->filter) {
                $search_filter = ($search->filter == 'equals') ? '=' : 'LIKE';
                $search_value = ($search->filter == 'equals') ? $search->value : '%'.$search->value.'%';
                $query->where($search->key, $search_filter, $search_value);
            }

            if ($orderBy && in_array($orderBy, $dataType->fields())) {
                $querySortOrder = (!empty($sortOrder)) ? $sortOrder : 'desc';
                $dataTypeContent = call_user_func([
                    $query->orderBy($orderBy, $querySortOrder),
                    $getter,
                ]);
            } elseif ($model->timestamps) {
                $dataTypeContent = call_user_func([$query->latest($model::CREATED_AT), $getter]);
            } else {
                $dataTypeContent = call_user_func([$query->orderBy($model->getKeyName(), 'DESC'), $getter]);
            }

            // Replace relationships' keys for labels and create READ links if a slug is provided.
            $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType);
        } else {
            // If Model doesn't exist, get data from table name
            $dataTypeContent = call_user_func([DB::table($dataType->name), $getter]);
            $model = false;
        }

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($model);

        // Eagerload Relations
        $this->eagerLoadRelations($dataTypeContent, $dataType, 'browse', $isModelTranslatable);

        // Check if server side pagination is enabled
        $isServerSide = isset($dataType->server_side) && $dataType->server_side;

        // Check if a default search key is set
        $defaultSearchKey = $dataType->default_search_key ?? null;

        // Actions
        $actions = [];
        if (!empty($dataTypeContent->first())) {
            foreach (Voyager::actions() as $action) {
                $action = new $action($dataType, $dataTypeContent->first());

                if ($action->shouldActionDisplayOnDataType()) {
                    $actions[] = $action;
                }
            }
        }

        // Define showCheckboxColumn
        $showCheckboxColumn = false;
        if (Auth::user()->can('delete', app($dataType->model_name))) {
            $showCheckboxColumn = true;
        } else {
            foreach ($actions as $action) {
                if (method_exists($action, 'massAction')) {
                    $showCheckboxColumn = true;
                }
            }
        }

        // Define orderColumn
        $orderColumn = [];
        if ($orderBy) {
            $index = $dataType->browseRows->where('field', $orderBy)->keys()->first() + ($showCheckboxColumn ? 1 : 0);
            $orderColumn = [[$index, $sortOrder ?? 'desc']];
        }

        $view = 'voyager::bread.browse';

        if (view()->exists("voyager::$slug.browse")) {
            $view = "voyager::$slug.browse";
        }

        if(isset($_GET['payment'])){
            $booking = Booking::with('payment')->whereNotIn('id', DB::table('payment')->pluck('book_id'))->where('client_id',$_GET['payment'])->get();
            return view('voyager::'.$slug.'.payments', compact('dataType','booking'));
        }else{
            return Voyager::view($view, compact(
                'actions',
                'dataType',
                'dataTypeContent',
                'isModelTranslatable',
                'search',
                'orderBy',
                'orderColumn',
                'sortOrder',
                'searchNames',
                'isServerSide',
                'defaultSearchKey',
                'usesSoftDeletes',
                'showSoftDeleted',
                'showCheckboxColumn'
            ));
        }
    }

    public function BookingPay(Request $request){
        foreach(json_decode($request->ids) as $key => $value) {
            $book = Booking::find($value);
            $payment = new Payment();
            $payment->amount = $book->total;
            $payment->type = $request->type;
            $payment->book_id = $value;
            $payment->client_id = $book->client_id;
            $payment->ref = $request->ref;
            $payment->status = 1;
            $payment->description = "pay success by Admin";
            $payment->save();
            Booking::where('id',$value)->update(['status' => 1]);
        }
        return redirect('admin/bookings');

    }

    public function CheckBook(Request $request)
    {
        $check = Booking::where('date',$request->date)
                        ->whereTime('from','>=',\Carbon\Carbon::parse($request->from))
                        ->whereTime('to','<=',\Carbon\Carbon::parse($request->to))
                        ->where('pitch',$request->pitch)
                        ->first();
        if($check === null){
            return response()->json([
                'state'=>true,
            ]);
        }else{
            return response()->json([
                'state'=>false,
                'msg' => 'This Pitch Aleardy Booked in this time',
            ]);
        }
    }

    public function CheckProduct($id)
    {
        $data = DB::table('products')->where('id',$id)->first();
        return response()->json([
            'data'=>$data,
        ]);
    }

    public function active(Request $request){
        $update=  DB::table('users')->where('id', $_GET['id'])->where('email', $_GET['email'])->where('activation_code', $_GET['code'])->update(['status' => 1]);
        return view('emails.welcome');
    }

    public function edite(Request $request){
       //dd($request->all());
        $date = $request['date'];
        $from = $request['from'];
        $to = $request['to'];
        $pitch = $request['pitch'];
        $size = $request['size'];
        $price = $request['price'];
        $discount = $request['discount'];
        $total = $request['total'];
        $sports =  $request['sports'];
        $id = $request['id'];

        $allSize = Booking::where('date',$date)->whereTime('from','<=',\Carbon\Carbon::parse($from))->whereTime('to','>=',\Carbon\Carbon::parse($to))->where('pitch',$pitch)->sum('size');
        $allSize += $size;
        if($allSize > 100){
            return response()->json([
                'state'=>400,
                'msg' => "you can't book over 100%",
            ]);              
        }

        $booking = Booking::find($id);
        $booking->date = $date;
        $booking->from = $from;
        $booking->to = $to;
        $booking->pitch = $pitch;
        $booking->size = $size;
        $booking->price = $price;
        $booking->dicsount = $discount;
        $booking->total = $total;
        $booking->client_id = $request->client_id;
        $booking->status = $request->status;
        $booking->penalty = $request->penalty;
        $booking->sports = $sports;
        $booking->code = rand ( 10000 , 99999 );
        $vat =  $price - round($price / 1.05);
        $booking->vat = $vat;  
        $booking->save();
        if(array_values($request->data[0]['title_product']) !== NULL){
            for($i= 0; $i < count($request->data[0]['title_product']); $i++){
                OrderProduct::where('book_id',$id)->delete();
                $OrderProduct = new OrderProduct();
                $OrderProduct->title = $request->data[0]['title_product'][$i];
                $OrderProduct->price = $request->data[0]['price_product'][$i];
                $OrderProduct->total = $request->data[0]['total_product'][$i];
                $OrderProduct->count = $request->data[0]['count_product'][$i];
                $OrderProduct->book_id  = $id;
                $OrderProduct->save();
                Product::where('id',$request->data[0]['title_product'][$i])->first()->decrement('stock', $request->data[0]['count_product'][$i]);

            }     
        } 
        return response()->json([
            'state'=>200,
            'msg' => "Data Insert Success",
        ]);
    }

    public function store(Request $request)
    {
        foreach($request->data as $key => $row)
        {
            $combinedarr[] = array("pitch"=>$row["pitch"],"date"=>$row["date"],"from"=>$row["from"]);
        }
        $countcomb = count($combinedarr);
        $uniquearr = array_unique($combinedarr, SORT_REGULAR);
        if($countcomb==count($uniquearr)){
            for($x= 0; $x < count($request->data); $x++){   
                $date = $request->data[$x]['date'];
                $from = $request->data[$x]['from'];
                $duration  = $request->data[$x]['hours'];
                $to = \Carbon\Carbon::parse($from)->addMinutes($duration)->format('H:i');
                //$to = $request->data[$x]['to'];
                $pitch = $request->data[$x]['pitch'];
                $size = $request->data[$x]['size'];
                $price = $request->data[$x]['price'];
                $discount = $request->data[$x]['discount'];
                $total = $request->data[$x]['total'];
                $sports = $request->data[$x]['sport'];
                $allSize = Booking::where('date',$date)->whereTime('from','<=',\Carbon\Carbon::parse($from))->whereTime('to','>=',\Carbon\Carbon::parse($to))->where('pitch',$pitch)->sum('size');
                $allSize += $size;
                if($allSize > 100){
                    return response()->json([
                        'state'=>400,
                        'msg' => "you can't book over 100%",
                    ]);              
                }
    
                $check = Booking::where('date',$date)->whereTime('from','>=',\Carbon\Carbon::parse($from))->whereTime('to','<=',\Carbon\Carbon::parse($to))->where('pitch',$pitch)->first();
                if($check === null){
                    $booking = new Booking();
                    $booking->date = $date;
                    $booking->from = $from;
                    $booking->to = $to;
                    $booking->pitch = $pitch;
                    $booking->size = $size;
                    $booking->price = $price;
                    $booking->dicsount = $discount;
                    $booking->total = $total;
                    $booking->client_id = $request->client_id;
                    $booking->status = $request->status;
                    $booking->penalty = $request->penalty;
                    $booking->sports = $sports;
                    $booking->code = rand ( 10000 , 99999 );
                    $vat =  $price - round($price / 1.05);
                    $booking->vat = $vat;  
                    $booking->status = 0; 
                    $booking->save();
                    if(array_values($request->data[$x]['title_product'])[0] !== NULL){
                        for($i= 0; $i < count($request->data[$x]['title_product']); $i++){
                            $OrderProduct = new OrderProduct();
                            $OrderProduct->title = $request->data[$x]['title_product'][$i];
                            $OrderProduct->price = $request->data[$x]['price_product'][$i];
                            $OrderProduct->total = $request->data[$x]['total_product'][$i];
                            $OrderProduct->count = $request->data[$x]['count_product'][$i];
                            $OrderProduct->book_id  = $booking->id;
                            $OrderProduct->save();
                            $product = Product::where('id',$request->data[$x]['title_product'][$i])->first();
                            $product->decrement('stock', $request->data[$x]['title_product'][$i]);
                        }     
                    }   
                }else{
                return response()->json([
                    'state'=>400,
                    'msg' => "This Pitch Aleardy Booked in this time",
                ]);    
                }
                $email = DB::table('users')->where('id',$booking->client_id)->value('email');
                $name = DB::table('users')->where('id',$booking->client_id)->value('name');            
                $this->MakePdfInvoice($booking->id);
                $this->MakePdfReceipt($booking->id);
                $details = [
                    'code' => $booking->code,
                    'date' => $booking->date,
                    'from' => $booking->from,
                    'to' => $booking->to,
                    'pitch' => $booking->pitch,
                    'sports' => $booking->sports,
                    'total' => $booking->total,
                    'id' => $booking->id,
    
                ];
                $email = DB::table('users')->where('id',$request->client_id)->value('email');
                //Mail::to($email)->send(new \App\Mail\Booking($details));      
            }  
        }else{
            return response()->json([
                'state'=>400,
                'msg' => "please do not repeat the pitch",
            ]); 
        };

               
        return response()->json([
            'state'=>200,
            'msg' => "Data Insert Success",
        ]);  
    }
    
    public function MakePdfInvoice($id){
        $booking = Booking::find($id);
        $details = [
            'code'  => $booking->code,
            'date'  => $booking->date,
            'from'  => $booking->from,
            'to'    => $booking->to,
            'pitch' => $booking->pitch,
            'sports'=> $booking->sports,
            'total' => $booking->total,
            'client_id' => $booking->client_id,
            'created_at'  => $booking->created_at,
            'id'  => $id,
        ];
        $pdf = PDF::loadView('emails.tax',compact('details'));
        return $pdf->save('attached/tax_'.$id.'.pdf');
    }

    public function MakePdfReceipt($id){
        $booking = Booking::find($id);
        $details = [
            'code'  => $booking->code,
            'date'  => $booking->date,
            'from'  => $booking->from,
            'to'    => $booking->to,
            'pitch' => $booking->pitch,
            'sports'=> $booking->sports,
            'total' => $booking->total,
            'client_id' => $booking->client_id,
            'id' => $id,
        ];
        $pdf = PDF::loadView('emails.invoice',compact('details'));
        return $pdf->save('attached/invoice_'.$id.'.pdf');
    }

    public function AddPay(Request $request)
    {
        $payment = new Payment();
        $payment->amount = $request->amount;
        $payment->type = $request->type;
        $payment->book_id = $request->book_id;
        $payment->client_id = $request->client_id;
        $payment->ref = $request->ref;
        $payment->description = "pay success by Admin";
        $payment->status = 1;
        $payment->rent = $request->rent;
        $payment->save();
        if($request->rent == 1){
            $sum = Payment::where('id',$request->book_id)->where('rent',1)->sum('amount');
            $total = Booking::where('id',$request->book_id)->value('price');
            if($sum == $total){
                Booking::where('id',$request->book_id)->update(['status' => 1]);
            }
            return redirect('admin/bookings?not_paid=1');
        }else{
            $sum = Payment::where('book_id',$request->book_id)->where('rent',2)->sum('amount');
            $toal = Subscription::where('id',$request->book_id)->value('subscription_fees');
            if($sum == $toal){
                Subscription::where('id',$request->book_id)->update(['paid' => 1]);
            }
            return redirect('admin/subscriptions');
        }



    }

    public function ExportInvoice($id)
    {
        $booking = Booking::find($id);
        $details = [
            'created_at'  => $booking->created_at,
            'date'  => $booking->date,
            'from'  => $booking->from,
            'to'    => $booking->to,
            'pitch' => $booking->pitch,
            'sports'=> $booking->sports,
            'total' => $booking->total,
            'client_id' => $booking->client_id,
            'code'  => $booking->code,
            'id' => $id, 
        ];
        //return view('emails.tax',compact('details'));
        $pdf = PDF::loadView('emails.tax',compact('details'));
        return $pdf->download('invoice'.$id.'.pdf');
    }

    public function ExportReceipt($id)
    {
        $booking = Booking::find($id);
        $name = DB::table('users')->where('id',$booking->client_id)->value('name');
        $email = DB::table('users')->where('id',$booking->client_id)->value('email');          
        $details = [
            'code'  => $booking->code,
            'date'  => $booking->date,
            'from'  => $booking->from,
            'to'    => $booking->to,
            'pitch' => $booking->pitch,
            'sports'=> $booking->sports,
            'total' => $booking->total,
            'name' => $name,
            'email' => $email,
            'date' => $booking->created_at, 
            'client_id' => $booking->client_id,
            'id' => $id,            
        ];
        //return view('emails.invoice',compact('details'));
        $pdf = PDF::loadView('emails.invoice',compact('details'));
        return $pdf->download('receipt'.$id.'.pdf');
    }

    public function ExportInvoiceProduct($id){
        $details = [
            'id'  => $id,
        ];
        $pdf = PDF::loadView('emails.invoice_product',compact('details'));
        return $pdf->download('invoice_product'.$id.'.pdf');
    }

    public function DeleteOrderProduct($id)
    {
        App\OrderProduct::where('id',$id)->delete();
        return redirect()->back();
    }

    public function Autocomplete(Request $request){
        $users = [];

        if($request->has('q')){
            $search = $request->q;
            $users =User::select("id", "name")
            		->where('name', 'LIKE', "%$search%")
            		->get();
        }
        return response()->json($users);
    }
}