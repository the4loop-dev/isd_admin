<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CourtTime;
use App\Court;
use DateTime;
use App\Court_books;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Events\BreadDataAdded;
use App\Court_times;
use Carbon;
use App\User;
use App\Notification;

class NotificationsController  extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{

    public function store(Request $request)
    {
        $token = User::where('id', $request->to)->value('token');
        //$token = "d264cab5-c380-444e-9876-44f82f12cf03";
        $msg = $request->message;
        $app_id = "aa840492-7879-433c-9f9b-55802a90045b";
        $key = "M2ExMDVmNDEtZDYwMy00N2ViLWEyYzAtZjM4N2ZjMTA0Nzlm";
        $content = array(
            "en" => 'Testing Message'
            );
    
            $fields = array(
                'app_id' => $app_id,
                'include_player_ids' => [$token],
                'large_icon' =>"ic_launcher_round.png",
                'userId', 'relation' => '=', 'value' => $token,
                'contents' => $content
            );
    
        $fields = json_encode($fields);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                                                   'Authorization: Basic M2ExMDVmNDEtZDYwMy00N2ViLWEyYzAtZjM4N2ZjMTA0Nzlm'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);    
    
        $response = curl_exec($ch);
        curl_close($ch);
        
        json_encode($response);  

        $Notification = new Notification();
        $Notification->to = $request->to;
        $Notification->message = $request->message;
        $Notification->save();
        return redirect('admin/notifications');
    
    }

}