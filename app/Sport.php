<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Sport extends Model
{
    protected $appends = ['iconPath','imagePath'];
   
    public function getimagePathAttribute($v)
    {
        return env('APP_URL').'/storage/'.$this->image;
    }

    public function geticonPathAttribute($v)
    {
        return env('APP_URL').'/storage/'.$this->icon;
    }

}
