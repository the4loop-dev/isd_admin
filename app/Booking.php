<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Booking extends Model
{

    protected $appends = ['sportName','courtName'];

    public function getCourtIdAttribute($value)
    {
        return Court::where('id',$value)->value('name');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'client_id');
    }

    public function products()
    {
        return $this->belongsTo('App\OrderProduct', 'book_id');
    }

    public function court()
    {
        return $this->belongsTo('App\Court', 'pitch');
    }

    public function getPitchAttribute($value)
    {
        return Court::where('id',$value)->value('name');
    }

    public function getTotalAttribute($value)
    {
        $OrderProduct = OrderProduct::where('book_id', $this->id)->sum('total');
        return $value + $OrderProduct;
    }

    public function getSizeAttribute($value)
    {
        return CourtsSize::where('size',$value)->value('name');
    }

    public function getStartusAttribute($value)
    {
        if($value == 1){
            $value  = "confirm";
        }else{
            $value  = "Not confirm";
        }
        return $value;

    }

    public function getMethodAttribute($value)
    {
        if($value == 0){
            $value  = "Cash";
        }else{
            $value  = "Visa";
        }
        return $value;

    }

    public function getSportNameAttribute($v)
    {
        $name = Sport::where('id',$this->sports)->value('name');
         return $name;
    }

    public function getCourtNameAttribute($v)
    {
        $name = Court::where('id',$this->pitch)->value('name');
         return $name;
    }

    public function payment(){
        return $this->belongsTo(Payment::class);
    }

}