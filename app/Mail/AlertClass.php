<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AlertClass extends Mailable
{
    use Queueable, SerializesModels;
    public $class;
    public $academy;
    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($class,$academy)
    {
        //
        $this->class  = $class;
        $this->academy  = $academy;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $class = $this->class ;
        $academy = $this->academy ;
        return $this->subject('Classes limit Nearing')->view('emails.alert_class');
    }
}
