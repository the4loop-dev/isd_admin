<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class CourtTime extends Model
{
 
    public function court()
    {
        return $this->belongsTo('App\Court', 'court_id', 'id');
    }
}
