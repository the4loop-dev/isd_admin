// JavaScript Document
$(function(){

    //*****************************
    // Reset Href
    //*****************************
    $('[href="#"]').attr("href","javascript:;");

    //*****************************
    // Smooth Scroll
    //*****************************
    function goToScroll(e){
        $('html, body').animate({
            scrollTop: $("."+e).offset().top
        }, 1000);
    }

    //*****************************
    // Lazy Load
    //*****************************
    $(window).scroll(function(){
        lazzyload();
    });


    //*****************************
    // Mobile Navigation
    //*****************************
    
    // if ($(window).width() <= 1200) {
    $('.mobile-nav-btn').click(function() {
        $('.mobile-nav-btn, .primary-header, .primary-navigation').toggleClass('nav-active');
        //$('.overlay-bg').fadeIn();
    });
    // }
    
    
    $('.mobile-click').click(function() {
        if ( $(this).siblings('.dropdown').hasClass( "show" ) ) {
            $(this).siblings('.dropdown').removeClass('show');
        } else {
            $('.dropdown').removeClass('show');
            $(this).siblings('.dropdown').addClass('show');
        }

        if ( $(this).hasClass( "active" ) ) {
            $('.mobile-click').removeClass('active');
            $(this).addClass('active');
        }
    });

    // $('.mobile-click').click(function() {
    //     if ( $(this).siblings('.dropdown').hasClass( "nav-active" ) ) {
    //         $(this).siblings('.dropdown').removeClass('nav-active');
    //     } else {
    //         $('.dropdown').removeClass('nav-active');
    //         $(this).siblings('.dropdown').addClass('nav-active');
    //     }
    // });


    $('.drop-menu').click(function(){
        $('.drop-menu').removeClass('active');
        $(this).addClass('active');
    });


    //*****************************
    // Close Funtion
    //*****************************
    $('.close-this').click(function(){
        $(this).parent().hide();
    });


    //*****************************
    // Scroll Funtion
    //*****************************

    if ($(window).width() <= 1200) {
        $('.video-section').on('inview', function(event, isInView) {
            var $this = $(this);
            if (isInView) {
                //console.log('play');
                $this.find("video").trigger('play');
            } else {
                //console.log('stop');
                $this.find("video").trigger('pause');
            }
        });
    };

    // $(window).scroll(function () {
    //     $('video[playonscroll]').each(function () {
    //         var videoElement = $(this)[0]; 
    //         var eTop = $(this).offset().top;
    //         var elementOffestY = (eTop - $(window).scrollTop());
    //         var videoOffset = [elementOffestY, elementOffestY+$(this).height()];
    //         if ((videoOffset[0] < 100) && (videoOffset[1] > 150)) {
    //             console.log('play');
    //             if (!videoElement.playing) {
    //                 videoElement.play();
    //             }
    //         } else {
    //             console.log('stop');
    //             if (videoElement.playing) {
    //                 videoElement.pause();
    //             }
    //         }
    //     });
    // });


    if ($(window).width() >= 1200) {
        var figure = $(".video-section");
        var vid = figure.find(".video-bg");

        [].forEach.call(figure, function (item,index) {
            item.addEventListener('mouseover', hoverVideo.bind(item,index), false);
            item.addEventListener('mouseout', hideVideo.bind(item,index), false);
        });

        function hoverVideo(index, e) {
            vid[index].play(); 
        }

        function hideVideo(index, e) {
            vid[index].pause(); 
        }
    }


    // $(window).scroll(function() {    
    //     var scroll = $(window).scrollTop();

    //     if ($(window).width() >= 1200) {
    //         if (scroll >= 10) {
    //             $('.primary-header').addClass('scroll');
    //             $('.mobile-nav-btn, .primary-header, .primary-navigation').removeClass('nav-active');
    //         } else {
    //             $('.primary-header').removeClass('scroll');
    //             //$('.mobile-nav-btn, .primary-header, .primary-navigation').addClass('nav-active');
    //         }
    //     }
    // });

    // if ($(window).width() >= 1200) {
    //     $('.homepg .mobile-nav-btn, .homepg .primary-header, .homepg .primary-navigation').addClass('nav-active');
    // } else {
    //     $('.homepg .mobile-nav-btn, .homepg .primary-header, .homepg .primary-navigation').removeClass('nav-active');
    // }


    //*****************************
    // Slick Slider
    //*****************************
    // $('.homepage-slider').slick({
    //     arrows: false,
    //     dots: false,
    //     infinite: true,
    //     autoplay:true,
    //     speed: 300,
    //     fade: true,
    //     cssEase: 'linear',
    //     slidesToShow: 1,
    //     slidesToScroll: 1
    // });


	var respsliders = {
      1: {slider : '.gallery-slider'},
      2: {slider : '.slider2'}
    };

    $.each(respsliders, function() {

        $(this.slider).slick({
            slidesToShow: 6,
            arrows: true,
            dots: false,
            infinite: true,
            autoplay:true,
            speed: 300,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        unslick: true,
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    });

    //*****************************
    // File Browser
    //*****************************
    xfilebrowse('.file-upload');
    function xfilebrowse(tgt){  
        $(tgt+' input[type=file]').each(function() {
            $(this).wrap('<div class="upldwrap" />');
            $(this).parent().append('<span class="browse">Choose Files</span> <label class="filelabel">Upload your files</label>');
            $(this).css('opacity', 0);
            $(this).on('change', function() {
                var txt = $(this).val();
                if(txt !== ''){
                    txt = txt.replace(/\\/g, '/').replace(/.*\//, '');
                    $(this).siblings('.filelabel').html(txt);
                }else{
                    $(this).siblings('.filelabel').html('No File Selected');
                }                
            })
        });
    }
    

    //*****************************
    // Fancybox
    //*****************************
    $('[data-fancybox="gallery"]').fancybox({
        // Options will go here
    });

    //*****************************
    // Form Animation
    //*****************************
    $('.form-field').on('focus blur',function(i){
        $(this).parents('.control-group').toggleClass('focused','focus'===i.type||this.value.length>0)
    }).trigger('blur');

    //*****************************
    // Tabbing
    //*****************************
    $('[data-targetit]').on('click',function () {
        $(this).siblings().removeClass('current');
        $(this).addClass('current');
        var target = $(this).data('targetit');
        $('.'+target).siblings('[class^="tabs"]').removeClass('show');
        $('.'+target).addClass('show');
        //$('.slick-slider').slick('setPosition', 0);
    });

    //*****************************
    // Modal
    //*****************************
    $('[data-targetmodal]').on('click',function () {
        $('body').addClass('modal-open');
        var target = $(this).data('targetmodal');
        $('.'+target).addClass('show');
    });

    $('.modal-close').click(function() {
        $('.x-modal').removeClass('show');
        $('body').removeClass('modal-open');
    });

    //*****************************
    // Accordian
    //*****************************
    $('.accordian-head').on('click',function () {
        $(this).parents().toggleClass('open');
        $(this).siblings('.accordian-body').slideToggle();
    });

    //*****************************
    // Copyright Year
    //*****************************
    now=new Date;thecopyrightYear=now.getYear();if(thecopyrightYear<1900)thecopyrightYear=thecopyrightYear+1900;$("#cur-year").html(thecopyrightYear);

    //*****************************
    // Set Map
    //*****************************
    $("address.setmap").each(function(){
        var embed ="<iframe frameborder='0' scrolling='no' marginheight='0' height='100%' width='100%' marginwidth='0' src='https://maps.google.com/maps?&amp;q="+ encodeURIComponent( $(this).text() ) +"&amp;output=embed'></iframe>";
        $(this).html(embed);
    });

    //*****************************
    // Click on Href Transition
    //*****************************
    (function(window) {
        'use strict';
        $.exists = function(selector) {
            return ($(selector).length > 0);
        }
        PageTransition();
    })(window);

    function PageTransition() {
        // var preload = anime({
        //     targets: '.ms-preloader',
        //     opacity: [1, 0],
        //     duration: 1000,
        //     easing: 'easeInOutCubic',
        //     complete: function(preload) {
        //         $('.ms-preloader').css('visibility', 'hidden');
        //     }
        // });
        // $("body").addClass('loading');
        //     $(".pre-loading").addClass('load');
        //     $(".spinner").fadeIn();
        // var cont = anime({
        //     targets: '.loaded',
        //     width: '0%'
        // });

        // $(document).on('click', '[data-type="page-transition"]', function(e) {
        //     e.preventDefault(); // prevent default anchor behavior
        //     var goTo = this.getAttribute("href"); // store anchor href

        //     $("body").removeClass('load');
        //     $("body").addClass('loading');
        //     $(".ms-loader").addClass('load');
        //     $(".spinner").fadeIn();

        //     setTimeout(function(){
        //         window.location = goTo;
        //     },1000);
        // });
    }

});


jQuery(document).ready(function($){
    orientationChange();
});

function orientationChange() {
    if(window.addEventListener) {
        window.addEventListener("orientationchange", function() {
            location.reload();
        });
    }
}