@php
    $edit = !is_null($dataTypeContent->getKey());
    $add  = is_null($dataTypeContent->getKey());
@endphp

@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular'))

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular') }}
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form"
                            class="form-edit-add"
                            action="{{ $edit ? route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) : route('voyager.'.$dataType->slug.'.store') }}"
                            method="POST" enctype="multipart/form-data" autocomplete="off">
                        <!-- PUT Method if we are editing -->
                        @if($edit)
                            {{ method_field("PUT") }}
                        @endif

                        <!-- CSRF TOKEN -->
                        {{ csrf_field() }}

                        <div class="panel-body">

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <!-- Adding / Editing -->
                            @php
                                $dataTypeRows = $dataType->{($edit ? 'editRows' : 'addRows' )};
                            @endphp

                            @foreach($dataTypeRows as $row)
                                <!-- GET THE DISPLAY OPTIONS -->
                                @php
                                    $display_options = $row->details->display ?? NULL;
                                    if ($dataTypeContent->{$row->field.'_'.($edit ? 'edit' : 'add')}) {
                                        $dataTypeContent->{$row->field} = $dataTypeContent->{$row->field.'_'.($edit ? 'edit' : 'add')};
                                    }
                                @endphp
                                @if (isset($row->details->legend) && isset($row->details->legend->text))
                                    <legend class="text-{{ $row->details->legend->align ?? 'center' }}" style="background-color: {{ $row->details->legend->bgcolor ?? '#f0f0f0' }};padding: 5px;">{{ $row->details->legend->text }}</legend>
                                @endif

                                <div class="form-group @if($row->type == 'hidden') hidden @endif col-md-{{ $display_options->width ?? 12 }} {{ $errors->has($row->field) ? 'has-error' : '' }}" @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif>
                                    {{ $row->slugify }}
                                    <label class="control-label" for="name">{{ $row->getTranslatedAttribute('display_name') }}</label>
                                    @include('voyager::multilingual.input-hidden-bread-edit-add')
                                    @if (isset($row->details->view))
                                        @include($row->details->view, ['row' => $row, 'dataType' => $dataType, 'dataTypeContent' => $dataTypeContent, 'content' => $dataTypeContent->{$row->field}, 'action' => ($edit ? 'edit' : 'add'), 'view' => ($edit ? 'edit' : 'add'), 'options' => $row->details])
                                    @elseif ($row->type == 'relationship')
                                        @include('voyager::formfields.relationship', ['options' => $row->details])
                                    @else
                                        {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
                                    @endif

                                    @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
                                        {!! $after->handle($row, $dataType, $dataTypeContent) !!}
                                    @endforeach
                                    @if ($errors->has($row->field))
                                        @foreach ($errors->get($row->field) as $error)
                                            <span class="help-block">{{ $error }}</span>
                                        @endforeach
                                    @endif
                                </div>
                            @endforeach

                        </div><!-- panel-body -->
                         





                </div>

                <div class="panel">
                    <div class="panel-body">
                        <div class="row">
                            <h4 class="" style="margin-left: 15px;"><i class=""></i>Prices</h4>
                            <div class="form-group  col-md-12 ">
                               <label class="control-label" for="name">Session Price</label>
                               <input type="text" class="form-control" name="price" placeholder="Package Name" value="<?= $edit ?  $dataTypeContent->price : '' ?>">
                            </div>                               
                            <div class="form-group  col-md-6 ">
                                <label class="control-label" for="name">One Day</label>
                                <input type="text" class="form-control" name="day1"  value="<?= $edit ? $dataTypeContent->day1 : '' ?>">
                            </div>
                            <div class="form-group  col-md-6 ">
                                <input type="checkbox" style="margin-top: 35px" id="day1_active" name="day1_active" <?= $edit && $dataTypeContent->day1_active == 1 ?  'checked'  : '' ?> <?= $edit && $dataTypeContent->day1_active == 1 ?  'value="true"'  : '' ?>>
                                <label class="control-label" for="name">Publish Package To Customers</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group  col-md-6 ">
                                <label class="control-label" for="name">Two Day</label>
                                <input type="text" class="form-control" name="day2" value="<?= $edit ?  $dataTypeContent->day2 : '' ?>">
                            </div>
                            <div class="form-group  col-md-6 ">
                                <input type="checkbox" style="margin-top: 35px" name="day2_active" id="day2_active"  <?= $edit && $dataTypeContent->day2_active == 1 ?  'checked'  : '' ?> <?= $edit && $dataTypeContent->day2_active == 1 ?  'value="true"'  : '' ?>>
                                <label class="control-label" for="name">Publish Package To Customers</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group  col-md-6 ">
                                <label class="control-label" for="name">Three Day</label>
                                <input type="text" class="form-control" name="day3"  value="<?= $edit ?  $dataTypeContent->day3 : '' ?>" >
                            </div>
                            <div class="form-group  col-md-6 ">
                                <input type="checkbox" style="margin-top: 35px" name="day3_active" id="day3_active"  <?= $edit && $dataTypeContent->day3_active == 1 ?  'checked'  : '' ?> <?= $edit && $dataTypeContent->day3_active == 1 ?  'value="true"'  : '' ?>>
                                <label class="control-label" for="name">Publish Package To Customers</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group  col-md-6 ">
                                <label class="control-label" for="name">Four Day</label>
                                <input type="text" class="form-control" name="day4"  value="<?= $edit ?  $dataTypeContent->day4 : '' ?>">
                            </div>
                            <div class="form-group  col-md-6 ">
                                <input type="checkbox" style="margin-top: 35px" name="day4_active" id="day4_active"  <?= $edit && $dataTypeContent->day4_active == 1 ?  'checked'  : '' ?> <?= $edit && $dataTypeContent->day4_active == 1 ?  'value="true"'  : '' ?>>
                                <label class="control-label" for="name">Publish Package To Customers</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group  col-md-6 ">
                                <label class="control-label" for="name">Five Day</label>
                                <input type="text" class="form-control" name="day5"  value="<?= $edit ?  $dataTypeContent->day5 : '' ?>">
                            </div>
                            <div class="form-group  col-md-6 ">
                                <input type="checkbox" style="margin-top: 35px" name="day5_active"  id="day5_active" <?= $edit && $dataTypeContent->day5_active == 1 ?  'checked'  : '' ?> <?= $edit && $dataTypeContent->day5_active == 1 ?  'value="true"'  : '' ?>>
                                <label class="control-label" for="name">Publish Package To Customers</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group  col-md-6 ">
                                <label class="control-label" for="name">Six Day</label>
                                <input type="text" class="form-control" name="day6"  value="<?= $edit ?  $dataTypeContent->day6 : '' ?>">
                            </div>
                            <div class="form-group  col-md-6 ">
                                <input type="checkbox" style="margin-top: 35px" name="day6_active" id="day6_active"  <?= $edit && $dataTypeContent->day6_active == 1 ?  'checked'   : '' ?> <?= $edit && $dataTypeContent->day6_active == 1 ?  'value="true"'  : '' ?>>
                                <label class="control-label" for="name">Publish Package To Customers</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group  col-md-6 ">
                                <label class="control-label" for="name">Seven Day</label>
                                <input type="text" class="form-control" name="day7"  value="<?= $edit ?  $dataTypeContent->day7 : '' ?>">
                            </div>
                            <div class="form-group  col-md-6 ">
                                <input type="checkbox" style="margin-top: 35px" name="day7_active" id="day7_active" <?= $edit && $dataTypeContent->day7_active == 1 ?  'checked'  : '' ?> <?= $edit && $dataTypeContent->day7_active == 1 ?  'value="true"'  : '' ?>>
                                <label class="control-label" for="name">Publish Package To Customers</label>
                            </div>
                        </div>

                    </div>

                        <div class="panel-footer">
                            @section('submit-buttons')
                                <button type="submit" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                            @stop
                            @yield('submit-buttons')
                        </div>

                </div>



            </form>

            <iframe id="form_target" name="form_target" style="display:none"></iframe>
            <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
                    enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                <input name="image" id="upload_file" type="file"
                         onchange="$('#my_form').submit();this.value='';">
                <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
                {{ csrf_field() }}
            </form>

            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
@stop

@section('javascript')
    <script>
        var params = {};
        var $file;

        function deleteHandler(tag, isMulti) {
          return function() {
            $file = $(this).siblings(tag);

            params = {
                slug:   '{{ $dataType->slug }}',
                filename:  $file.data('file-name'),
                id:     $file.data('id'),
                field:  $file.parent().data('field-name'),
                multi: isMulti,
                _token: '{{ csrf_token() }}'
            }

            $('.confirm_delete_name').text(params.filename);
            $('#confirm_delete_modal').modal('show');
          };
        }

        $('document').ready(function () {
            $('#day1_active').change(function(){
                cb = $(this);
                cb.val(cb.prop('checked'));
            });
            $('#day2_active').change(function(){
                cb = $(this);
                cb.val(cb.prop('checked'));
            });
            $('#day3_active').change(function(){
                cb = $(this);
                cb.val(cb.prop('checked'));
            });
            $('#day4_active').change(function(){
                cb = $(this);
                cb.val(cb.prop('checked'));
            });
            $('#day5_active').change(function(){
                cb = $(this);
                cb.val(cb.prop('checked'));
            });
            $('#day6_active').change(function(){
                cb = $(this);
                cb.val(cb.prop('checked'));
            });
            $('#day7_active').change(function(){
                cb = $(this);
                cb.val(cb.prop('checked'));
            });                                                            
            $('.toggleswitch').bootstrapToggle();
            //Init datepicker for date fields if data-datepicker attribute defined
            //or if browser does not handle date inputs
            $('.form-group input[type=date]').each(function (idx, elt) {
                if (elt.hasAttribute('data-datepicker')) {
                    elt.type = 'text';
                    $(elt).datetimepicker($(elt).data('datepicker'));
                } else if (elt.type != 'date') {
                    elt.type = 'text';
                    $(elt).datetimepicker({
                        format: 'L',
                        extraFormats: [ 'YYYY-MM-DD' ]
                    }).datetimepicker($(elt).data('datepicker'));
                }
            });

            @if ($isModelTranslatable)
                $('.side-body').multilingual({"editing": true});
            @endif

            $('.side-body input[data-slug-origin]').each(function(i, el) {
                $(el).slugify();
            });

            $('.form-group').on('click', '.remove-multi-image', deleteHandler('img', true));
            $('.form-group').on('click', '.remove-single-image', deleteHandler('img', false));
            $('.form-group').on('click', '.remove-multi-file', deleteHandler('a', true));
            $('.form-group').on('click', '.remove-single-file', deleteHandler('a', false));

            $('#confirm_delete').on('click', function(){
                $.post('{{ route('voyager.'.$dataType->slug.'.media.remove') }}', params, function (response) {
                    if ( response
                        && response.data
                        && response.data.status
                        && response.data.status == 200 ) {

                        toastr.success(response.data.message);
                        $file.parent().fadeOut(300, function() { $(this).remove(); })
                    } else {
                        toastr.error("Error removing file.");
                    }
                });

                $('#confirm_delete_modal').modal('hide');
            });
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@stop

@section('javascript')
    <script>
        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();
        });
    </script>
@stop
