@extends('layouts.user')
@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Bookings</h1>
    </div>
    
    <div class="row">
        <div class="col-lg-12">
            <!-- Default Card Example -->
            <div class="card mb-4">
                <div class="card-header">
                    Select Court
                </div>
                <div class="card-body">
                    {!! Form::open(['route' => 'client.check.courts', 'files' => 'true', 'method' => 'POST', 'enctype'=>"multipart/form-data"]) !!}
                    
                    <div class="form-group">
                        <label for="date" class="form-label">{{ __('Date') }}</label>
                        {{ Form::date('date',$request->date ?? null, ['class' => 'form-control','id'=>'date','autocomplete'=>"off"]) }}
                        @error('date')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="time" class="form-label">{{ __('Time From') }}</label>
                        {{ Form::time('time',$request->time ?? null, ['class' => 'form-control','id'=>'time','autocomplete'=>"off"]) }}
                        @error('time')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="to" class="form-label">{{ __('Time To') }}</label>
                        {!! Form::label('Duration', 'Duration', ['class' => 'form-label']) !!}
                        {{ Form::select('duration',$durations,$request->duration ?? null, ['class' => 'form-control','placeholder'=>"Select Duration"]) }}
                        @if ($errors->has('duration'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('duration') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        
                        {!! Form::label('sport', 'Select sport', ['class' => 'form-label']) !!}
                        {{ Form::select('sport',$sports,$request->sport ?? null, ['class' => 'form-control','placeholder'=>"Select Option"]) }}
                        @if ($errors->has('sport'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('sport') }}</strong>
                        </span>
                        @endif
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                    
                    
                    
                </div>
            </div>
            
        </div>
    </div>
</div>
<!-- /.container-fluid -->
@endsection